# Currency Converter BE

> Serves cached currency exchange rates from the Czech National Bank (CNB) API.

## Deployment

- Expected to be deployed at [momence-cc-be.vercel.app](https://momence-cc-be.vercel.app/).
- In case this project is deployed as a serverless function
  on e.g. [Vercel](https://vercel.com/guides/using-express-with-vercel#standalone-express),
  we enable caching on the API endpoint, so the CDN can cache the response for 1 hour and serve it fast.
  Note that the rates at CNB are only updated once a day.

## Notes

- Calls the
  [CNB API](https://www.cnb.cz/en/financial-markets/foreign-exchange-market/central-bank-exchange-rate-fixing/central-bank-exchange-rate-fixing/daily.txt)
  (see its documentation [here](https://www.cnb.cz/en/faq/Format-of-the-foreign-exchange-market-rates/)).

## Known Issues and Limitations

- Allows any origin in the CORS policy which is not a good practice; it should be limited to a list of allowed origins.
- Should have proper REST API documentation exposed.
- For deployments on Vercel, using Node.js and Express is not the best practice, as it is not meant for serverless functions,
  but was chosen for simplicity and familiarity.
- The project could use tests, especially unit tests for the CNB data parser.
