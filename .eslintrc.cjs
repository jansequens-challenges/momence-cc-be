module.exports = {
    root: true,
    env: {browser: true, es2020: true},
    extends: [
        'eslint:recommended',
        'plugin:@typescript-eslint/recommended-type-checked',
        'plugin:@typescript-eslint/stylistic-type-checked',
    ],
    ignorePatterns: ['dist', '.eslintrc.cjs'],
    parser: '@typescript-eslint/parser',
    rules: {
        '@typescript-eslint/explicit-function-return-type': ['warn'],
        '@typescript-eslint/no-explicit-any': ['warn'],
        'func-style': ['error', 'expression'],
        'max-lines': ['error', 400],
        "indent": ["error", 4],
        "linebreak-style": ["error", "unix"]
    },
    parserOptions: {
        ecmaVersion: 'latest',
        sourceType: 'module',
        project: ['./tsconfig.json'],
        tsconfigRootDir: __dirname
    }
}
