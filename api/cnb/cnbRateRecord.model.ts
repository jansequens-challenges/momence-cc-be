export interface CnbRateRecord {
    country: string;
    currencyName: string;
    currencyCode: string;
    amount: number;
    rate: number;
}
