import {CnbRateRecord} from './cnbRateRecord.model';
import {CnbRates} from './cnbRates.model';

// See CNB API docs: https://www.cnb.cz/en/faq/Format-of-the-foreign-exchange-market-rates/

/*
 Parses the CNB rates from a string response.
 */
export const parseCnbRates = (data: string): CnbRates => {
    const lines = data.split('\n');

    // first line is the date
    const date = parseDateLine(lines[0]);

    // second line is the header - ignore it

    // the rest are currencies
    const records = lines
        .slice(2) // ignore the date and header
        .filter(line => line.length > 0) // filter out empty lines
        .map(line => parseCurrencyLine(line));

    return {date, records};
}

/*
 Parses a line that starts with DD.MM.YYYY and returns an ISO date (YYYY-MM-DD).

 Note that while the documentation states dot-separated format (e.g., "03.Jan.2000 #1"),
 the API actually returns a space-separated format (e.g., "03 Jan 2000 #1").
 To be on the safe side, we're able to handle both formats.
 */
const parseDateLine = (line: string): string => {
    const dateString = line.split('#')[0].trim();
    const separator = dateString.indexOf('.') > 0 ? '.' : ' ';
    const chunks = dateString.split(separator);
    if (chunks.length !== 3) {
        throw new Error(`Invalid date line format: ${line}`);
    }
    // convert to date to check validity
    const dateObj = new Date(Date.parse(`${chunks[0]} ${chunks[1]} ${chunks[2]}`));

    // return as YYYY-MM-DD
    return dateObj.toISOString().substring(0, 10);
}

/*
 Parses a line for one currency (e.g. 'USA|dollar|1|USD|22.554')
 */
const parseCurrencyLine = (line: string): CnbRateRecord => {
    const parts = line.split('|');
    if (parts.length !== 5) {
        throw new Error(`Invalid currency line format: ${line}`);
    }

    return {
        country: parts[0],
        currencyName: parts[1],
        currencyCode: parts[3],
        amount: parseInt(parts[2]),
        rate: parseFloat(parts[4]),
    };
}
