import express, {Request, Response} from 'express'
import axios, {AxiosError} from 'axios'
import cors from 'cors';
import {parseCnbRates} from './cnb/cnbParser';
import {ApiError} from './apiError.model';

const defaultErrorStatus = 500;
const cacheExpirationSec = 3600;
const cnbRatesUrl =
    'https://www.cnb.cz/en/financial-markets/foreign-exchange-market' +
    '/central-bank-exchange-rate-fixing/central-bank-exchange-rate-fixing/daily.txt'

const app = express();
app.use(cors()); // allows any origin

// gets rates from the Czech National Bank (CNB)
app.get('/api/rates', (_req: Request, res: Response) => {

    res.setHeader(`Cache-Control`, `s-max-age=${cacheExpirationSec}, stale-while-revalidate`);
    axios
        .get<string>(cnbRatesUrl)
        .then((cnbResponse) => {
            try {
                return res.send(parseCnbRates(cnbResponse.data));
            } catch (e: any) {
                const apiError: ApiError = {message: `Could not parse the CNB response: ${e.toString()}`};
                return res.send(apiError);
            }
        })
        .catch((error: Error | AxiosError) => {
            // forward the status from downstream
            axios.isAxiosError(error) && error.response?.status ? res.status(error.response.status) : res.status(defaultErrorStatus);
            // forward the error
            const apiError: ApiError = {message: error.toString()};
            return res.send(apiError);
        })
});

// for local development only if deployed as a serverless function on e.g. Vercel
app.listen(5000, () => {
    console.log('Running on port 5000.');
});

module.exports = app;
